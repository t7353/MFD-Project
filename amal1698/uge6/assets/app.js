// OPGAVE 2.2 //
// const app = Vue.createApp({
//   // data, functions, methods
//   //  template: '<h2>I am the template</h2>'
//   data(){
//     return{
//       showBooks: true,
//       title: 'Harry Potter and the Philosophers Stone',
//       author: 'J. K. Rowling',
//       year:  1997
//     }
//   },
//   methods: {
//     /* changeTitle(title){
//        //this.title = 'Harry Potter and the Chamber of Secrets'
//        this.title = title
//     } */
//     toggleShowBook(){
//       this.showBooks = !this.showBooks
//     }
//   }
// })
//
// app.mount('#app')



// OPGAVE 2.3 //
const app = Vue.createApp({
  data(){
    return{
      url: 'https://www.ucl.dk',
      showBooks: true,
      books:[
        {title: 'Harry Potter and the Philosophers Stone', author: 'J. K. Rowling', year: 1997, publisher: 'Bloomsbury', img:'assets/img/hp1.jpeg'},
        {title: 'Harry Potter and Chamber of Secrets', author: 'J. K. Rowling', year: 1998, publisher: 'Bloomsbury', img:'assets/img/hp2.jpeg'},
        {title: 'Harry Potter and the Prisoner of Azkaban', author: 'J. K. Rowling', year: 1999, publisher: 'Bloomsbury', img:'assets/img/hp3.jpeg', isFav: true},
        {title: 'Harry Potter and the Goblet of Fire', author: 'J. K. Rowling', year: 2000, publisher: 'Bloomsbury', img:'assets/img/hp4.jpeg'},
        {title: 'Harry Potter and the Order of the Phoenix', author: 'J. K. Rowling', year: 2003, publisher: 'Bloomsbury', img:'assets/img/hp5.jpeg'},
        {title: 'Harry Potter and the Half-Blood Prince', author: 'J. K. Rowling', year: 2005, publisher: 'Bloomsbury', img:'assets/img/hp6.jpeg'},
        {title: 'Harry Potter and the Deathly Hallows', author: 'J. K. Rowling', year: 2007, publisher: 'Bloomsbury', img:'assets/img/hp7.jpeg'}
      ]
      // title: 'Harry Potter and the Philosophers Stone',
      // author: 'J. K. Rowling',
      // year:  1997,
      // x: 0,
      // y: 0
    }
  },
  methods: {
  toggleShowBook(){
      this.showBooks = !this.showBooks
    },
    // handleEvent(e, data){
    //   console.log(e, e.type)
    //   if (data){
    //     console.log(data)
    //   }
    // },
    // handleMousemove(e){
    //   this.x = e.offsetX
    //   this.y = e.offsetY
    // }
  toggleFav(book){
    book.isFav = !book.isFav
  }
},

computed:{
  filteredBooks(){
    return this.books.filter((book)=>book.isFav)
  }
}

})

app.mount('#app')
