# Fetching from strapi with Axios
1. import

´´´ vue

<template>
<div v-if="error">
  {{ error }}
</div>
<ul v-else>
  <li v-for="category in categories" :key="category.id">
    {{ category.attributes.titletext.title }}
  </li>
</ul>
</template>
<script>
import axios from 'axios';
axios.get('http://localhost:1337/api/categories?populate=%2A').then(response => {
  console.log(response);
});
export default {
    data () {
    return {
      categories: [],
      error: null
    }
  },
  async mounted () {
    try {
      const response = await axios.get('http://localhost:1337/api/categories?populate=%2A')
      this.categories = response.data.data
    } catch (error) {
      this.error = error;
    }
  }
}

</script>
´´´
