# Opgaver 6 - Markdown
This is a link - [Littleplay](https://www.littleplay.netlify.app)

![Logo](littleplay_logo.png)

Indsæt et billede (link til et billede)
[![This is logo](littleplay_logo.png)](https://littleplay.netlify.app/)

| You    | can      | create  | tables    |
| :---   |  :----:  |  :----: |      ---: |
| with  -| pipes \| | and     | hyphens --|
| hyphens| Create   | columns | header    |
| pipes  | separate | each    | columns   |

>Lav en unordered list (bullet points) med 3 niveauer
> - First Level
>    - Second Level
>       - Third Level - now I understand the question!

>Lav en ordered list med 2 niveauer
>
> 1. First Level
>       2. Second Level

»This line contains a tab character.

>Lav fed tekst og italic tekst. Kan man lave fed italic tekst >samtidig?
<br>
>
> ***Bold and Italic***	
> **Hello You!** 
> *Italic Markdown*

#### JS code block
```js
const season = winter
  && winter.elements
  && winter.elements.snow
  && winter.elements.snow.state
  && winter.elements.snow.state.temperature
  && winter.elements.snow.state.temperature.celsius;
```
Find selv på mere hvis du har ekstra tid. Kig evt. på bonus herunder.

The winter has `sparkling` and frozen `elements`!

* Winter
  ```jsx
  const Snow = <Snowflake amount=20 />;
  ```
* Frost

## Very Cool Markdown
### Author Edna Valter